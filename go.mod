module gitlab.com/begraf/doccat

go 1.15

require (
	github.com/AlecAivazis/survey/v2 v2.2.4
	github.com/fsnotify/fsnotify v1.4.7
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v1.1.1
	github.com/spf13/viper v1.7.1
	gopkg.in/yaml.v2 v2.2.8
)
