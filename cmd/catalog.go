package cmd

import (
	"fmt"
	"gitlab.com/begraf/doccat/catalog"
	"io"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/spf13/cobra"
	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
)

// catalogCmd represents the catalog command
var catalogCmd = &cobra.Command{
	Use:   "catalog",
	Short: "Query metadata and copy the given file",
	Long: "",
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return fmt.Errorf("expected one argument <filepath>")
		}
		stat, err := os.Stat(args[0])
		if err != nil {
			return fmt.Errorf("could not stat given file: %w", err)
		}
		if !stat.Mode().IsRegular() {
			return fmt.Errorf("given path is not a regular file")
		}
		return nil
	},
	Run: func(cmd *cobra.Command, args []string) {
		runCatalog(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(catalogCmd)

	viper.Set("filename_separator", "__")
}

func runCatalog(cmd *cobra.Command, args []string) {
	log := log.New(os.Stderr, "[catalog] ", log.LstdFlags)

	// Collect target directories
	targets, err := gatherTargetDirectories()
	if err != nil {
		log.Fatalf("gathering targets: %w", err)
	}

	sourceFilename := args[0]
	log.Printf("source file: %s", sourceFilename)

	// Get a temporary query file
	queryFile, err := ioutil.TempFile("", "doccat-tagcopy-*.yaml")
	if err != nil {
		log.Fatalf("could not create temporary file: %s", err)
	}
	defer func (){
		err := os.Remove(queryFile.Name())
		if err != nil {
			log.Fatalf("removing query file: %s", err)
		}
	}()
	log.Printf("opened temporary query file at: %s", queryFile.Name())

	// Write query file
	err = writeQueryFile(targets, queryFile)
	if err != nil {
		log.Fatalf("writing query file: %s", err)
	}
	queryFile.Close()

	// Loop: query metadata until it is complete and the job done
	for {
		// Run editor on query file
		err = runEditor(queryFile.Name())
		if err != nil {
			log.Fatalf("editor-command: %s", err)
		}

		// Read query file
		metadata, err := deserializeQueryFile(queryFile.Name())
		if err != nil {
			log.Printf("reading query file: %s", err)
			if promptYesNo("Continue") {
				continue
			}
			os.Exit(1)
		}

		// Check that the target is a directory
		target := metadata.Target
		err = validateTarget(target)
		if err != nil {
			log.Printf("validate target: %s", err)
			if promptYesNo("Continue") {
				continue
			}
			os.Exit(2)
		}

		// Create filename
		filenamePrefix, err := catalog.MakeFilename(metadata, viper.GetString("filename_separator"))
		if err != nil {
			log.Printf("could not generate filename: %s", err)
			if promptYesNo("Continue") {
				continue
			}
			os.Exit(2)
		}

		extension := filepath.Ext(sourceFilename)
		targetFilename := filepath.Join(target, filenamePrefix + extension)

		fmt.Printf("Source: %s\n", sourceFilename)
		fmt.Printf("Target: %s\n", targetFilename)
		if !promptYesNo("Copy") {
			// Don't copy..
			if promptYesNo("Continue") {
				continue
			}
			os.Exit(4)
		}

		// Perform the copy
		err = copyFile(sourceFilename, targetFilename)
		if err != nil {
			log.Printf("cp (%s) → (%s): %w", sourceFilename, targetFilename, err)
			if promptYesNo("Edit again") {
				continue
			}
			os.Exit(5)
		}

		break
	}
}

func copyFile(src, dst string) error {
	copyCmd := exec.Command("cp", src, dst)
	return copyCmd.Run()
}

func runEditor(path string) error {
	editorCmd := exec.Command("nvim", path)
	editorCmd.Stdin = os.Stdin
	editorCmd.Stdout = os.Stdout
	editorCmd.Stderr = os.Stderr
	err := editorCmd.Run()
	if err != nil {
		return fmt.Errorf("editor-command: %s", err)
	}
	return nil
}

func validateTarget(path string) error {
	stat, err := os.Stat(path)
	if err != nil {
		return fmt.Errorf("could not stat target '%s': %w", path, err)
	} else if !stat.IsDir() {
		return fmt.Errorf("target is not a directory")
	}
	return nil
}

func deserializeQueryFile(path string) (catalog.DocumentMetadata, error) {
	var metadata catalog.DocumentMetadata
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return metadata, fmt.Errorf("read: %w", err)
	}
	err = yaml.Unmarshal(data, &metadata)
	if err != nil {
		return metadata, fmt.Errorf("parse: %w", err)
	}
	return metadata, nil
}

func promptYesNo(message string) bool {
	tryAgain := true
	prompt := &survey.Confirm{
		Message: message,
		Default: tryAgain,
	}
	survey.AskOne(prompt, &tryAgain)
	return tryAgain
}

func writeQueryFile(targets []string, to io.Writer) error {
	fmt.Fprint(to, "title: \n\n")

	fmt.Fprintln(to, "# date [yyyy[-mm-dd]]")
	fmt.Fprint(to, "date: \n\n")

	fmt.Fprintln(to, "# natural person:              <surname>, <givenname>")
	fmt.Fprintln(to, "# groups, organizations, etc.: <name>")
	fmt.Fprint(to, "authors:\n- \n\n")

	// Targets
	for _, targetDirectory := range targets {
		fmt.Fprintf(to, "#target: %s\n", targetDirectory)
	}

	return nil
}

func gatherTargetDirectories() ([]string, error) {
	var directories []string

	// Set of excluded paths for quick and easy lookup
	excludedPath := make(map[string]struct{})
	for _, p := range viper.GetStringSlice("target_excludes") {
		absPath, err := filepath.Abs(p)
		if err != nil {
			return nil, fmt.Errorf("absolute path recovery for '%s' failed: %w", p, err)
		}
		excludedPath[absPath] = struct{}{}
	}

	for _, root := range viper.GetStringSlice("target_roots") {
		dirEntries, err := ioutil.ReadDir(root)
		if err != nil {
			return nil, fmt.Errorf("could not read directory '%s': %w", root, err)
		}

		for _, fileinfo := range dirEntries {
			if fileinfo.IsDir() {
				path := filepath.Join(root, fileinfo.Name())
				absPath, err := filepath.Abs(path)
				if err != nil {
					return nil, fmt.Errorf("absolute path recovery for '%s' failed: %w", path, err)
				}

				// if the path is not excluded, then add it
				if _, ok := excludedPath[absPath]; !ok {
					directories = append(directories, absPath)
				}
			}
		}
	}

	return directories, nil
}