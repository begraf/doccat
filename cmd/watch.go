package cmd

import (
	"github.com/fsnotify/fsnotify"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"log"
	"os"
	"os/exec"
	"time"
)

// watchCmd represents the watch command
var watchCmd = &cobra.Command{
	Use:   "watch",
	Short: "Watch configured directories for incoming files",
	Long: "",
	Run: func(cmd *cobra.Command, args []string) {
		run(cmd, args)
	},
}

func init() {
	rootCmd.AddCommand(watchCmd)
}

func run(cmd *cobra.Command, args []string) {
	log := log.New(os.Stderr, "[watch] ", log.LstdFlags)

	watcher, err := fsnotify.NewWatcher()
	if err != nil {
		log.Fatalf(
			"watch: could not create watcher: %w",
			err,
		)
	}
	defer watcher.Close()

	eventCh := make(chan string)
	go func() {
		for {
			select {
			case event, ok := <-watcher.Events:
				if !ok {
					return
				}
				if event.Op&fsnotify.Write == fsnotify.Write {
					eventCh <- event.Name
				}

			case err, ok := <-watcher.Errors:
				if !ok {
					return
				}
				log.Println("error:", err)
			}
		}
	}()

	// Setup directories to watch
	directories := viper.GetStringSlice("watch_directories")
	if len(directories) == 0 {
		log.Fatalf("no directories to watch specified under key 'watch_directories'")
	}
	for _, directory := range directories {
		err = watcher.Add(directory)
		if err != nil {
			log.Fatalf("could not add '%s' to watcher: %w", directory, err)
		}
	}

	// Monitor changes
	candidates := make(map[string]time.Time)
	for {
		timeout := time.Millisecond * 100
		if len(candidates) == 0 {
			timeout = time.Hour * 24
		}

		select {
		case path := <-eventCh:
			candidates[path] = time.Now()
		case <-time.After(timeout):
		}

		// Check whether candidates are ready
		now := time.Now()
		for path, lastTime := range candidates {
			nonModifiedDuration := now.Sub(lastTime)
			if nonModifiedDuration.Milliseconds() > 100 { // TODO: remove hardcoded limit
				// The path has not been modified long enough, we can emit
				log.Println("registered:", path)
				delete(candidates, path)
				executeCatalog(path)
			}
		}
	}
}

func executeCatalog(path string) {
	go func() {
		log := log.New(os.Stderr, "[execute-catalog] ", log.LstdFlags)
		cmd := makeCatalogCommand(path)
		log.Printf("starting on: %s", path)
		err := cmd.Run()
		if err != nil {
			log.Println(err)
		}
	}()
}

func makeCatalogCommand(path string) *exec.Cmd {
	commandName := os.Args[0]
	cmd := exec.Command(
		"xfce4-terminal",
		"--hide-menubar",
		"--title=doccat - tagcopy",
		"-x",
		commandName,
		"catalog",
		path,
	)
	return cmd
}

