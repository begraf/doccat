# doccat

*Doccat* watches given directories for newly added files.
When a new file is added, a process is started querying certain metadata
and a target directory.
The file is copied into the target directory with a name reflecting
the provided metadata (title, author, date).

I use it for quick organization of PDFs I save (e.g., invoices, blog articles).
