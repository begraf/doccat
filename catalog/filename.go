package catalog

import (
	"fmt"
	"regexp"
	"strings"
	"time"
)

func MakeFilename(metadata DocumentMetadata, sep string) (string, error) {
	var parts []string
	if authorStr, ok := MakeFilenameAuthor(metadata.Authors); ok {
		parts = append(parts, authorStr)
	}
	if titleStr, ok := makeFilenameTitle(metadata.Title); ok {
		parts = append(parts, titleStr)
	}
	if len(parts) == 0 {
		return "", fmt.Errorf("neither AUTHOR nor TITLE given")
	}
	parts = append(parts, time.Now().Format("2006-01-02"))
	return strings.Join(parts, sep), nil
}

func makeFilenameTitle(title string) (string, bool) {
	titleStr := makeFilenameComponent(title)
	if len(titleStr) == 0 {
		return "", false
	}
	return titleStr, true
}

func MakeFilenameAuthor(authors []string) (author string, ok bool) {
	writePos := 0
	for _, author := range authors {
		trimmedAuthor := strings.TrimSpace(author)
		if len(trimmedAuthor) > 0 {
			authors[writePos] = trimmedAuthor
			writePos++
		}
	}
	authors = authors[:writePos]

	switch len(authors) {
	case 0:
		return "", false
	case 1:
		return getShortAuthor(authors[0]), true
	case 2:
		return fmt.Sprintf("%s_and_%s", getShortAuthor(authors[0]), getShortAuthor(authors[1])), true
	default:
		return fmt.Sprintf("%s_et_al", getShortAuthor(authors[0])), true
	}
}

func getShortAuthor(author string) string {
	parts := strings.Split(author, ",")
	if len(parts) != 2 {
		return makeFilenameComponent(author)
	}
	return makeFilenameComponent(parts[0])
}

func makeFilenameComponent(input string) string {
	specials := regexp.MustCompile(`[^\w\s-]+`)
	space := regexp.MustCompile(`\s+`)
	input = strings.TrimSpace(input)
	input = specials.ReplaceAllString(input, "")
	return space.ReplaceAllString(input, "_")
}

