package catalog

type DocumentMetadata struct {
	Title   string   `yaml:"title"`
	Authors []string `yaml:"authors"`
	Target  string   `yaml:"target"`
	DateStr string   `yaml:"date"`
}


