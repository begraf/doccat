package catalog

import (
	"fmt"
	"testing"
	"time"
)

func TestMakeFilename(t *testing.T) {
	testcases := []struct {
		Metadata DocumentMetadata
		Expected string
	}{
		{
			Metadata: DocumentMetadata{
				Authors: []string{"Twain"},
				Title: "Some musings about some nonsense",
			},
			Expected: fmt.Sprintf("Twain__Some_musings_about_some_nonsense__%s", time.Now().Format("2006-01-02")),
		},
	}

	for _, testcase := range testcases {
		actual, _ := MakeFilename(testcase.Metadata, "__")
		if actual != testcase.Expected {
			t.Errorf("MakeFilename(%v) = %v; want %v", testcase.Metadata, actual, testcase.Expected)
		}
	}
}

func TestMakeFilenameFailNoAuthorTitle(t *testing.T) {
	_, err := MakeFilename(DocumentMetadata{}, "__")
	if err == nil {
		t.Errorf("expected error")
	}
}

func TestFilenameAuthor(t *testing.T) {
	testcases := []struct {
		Authors  []string
		Expected string
	}{
		{
			Authors:  []string{"Wilde, Oscar"},
			Expected: "Wilde",
		},
		{
			Authors:  []string{"Wilde, Oscar", "Twain,  Mark"},
			Expected: "Wilde_and_Twain",
		},
		{
			Authors:  []string{"Wilde, Oscar", "Twain,  Mark", "Zappa, Frank"},
			Expected: "Wilde_et_al",
		},
		{
			Authors:  []string{"Legitimate Business Inc."},
			Expected: "Legitimate_Business_Inc",
		},
	}

	for _, testcase := range testcases {

		actual, _ := MakeFilenameAuthor(testcase.Authors)
		if actual != testcase.Expected {
			t.Errorf("MakeFilenameAuthor(%v) = %v; want %v", testcase.Authors, actual, testcase.Expected)
		}
	}
}
